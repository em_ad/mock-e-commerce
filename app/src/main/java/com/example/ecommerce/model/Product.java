package com.example.ecommerce.model;

import androidx.annotation.Nullable;

import java.io.Serializable;

public class Product implements Serializable {

    String name;
    Long price;

    public String getName() {
        return name;
    }

    public Long getPrice() {
        return price;
    }

    public Product(String name, Long price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) return false;
        return name.equals(((Product) obj).name) &&
                price.equals(((Product) obj).price);
    }
}
