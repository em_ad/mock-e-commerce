package com.example.ecommerce.model;

public class Event {

    EventType type;
    Object payload;

    public Event(EventType type, Object payload) {
        this.type = type;
        this.payload = payload;
    }

    public EventType getType() {
        return type;
    }

    public Object getPayload() {
        return payload;
    }
}
