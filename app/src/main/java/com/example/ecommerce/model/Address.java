package com.example.ecommerce.model;


import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = "address_db")
public class Address {

    @ColumnInfo(name = "country")
    private String country;
    @ColumnInfo(name = "state")
    private String state;
    @ColumnInfo(name = "city")
    private String city;
    @ColumnInfo(name = "street")
    private String street;
    @PrimaryKey
    @NotNull
    private String postal;

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public String getPostal() {
        return postal;
    }

    public Address(String country, String state, String city, String street, String postal) {
        this.country = country;
        this.state = state;
        this.city = city;
        this.street = street;
        this.postal = postal;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null)
            return false;
        Address address = (Address) obj;
        if (address.getPostal().equals(postal) &&
                address.getCountry().equals(country) &&
                address.getCity().equals(city) &&
                address.getStreet().equals(street) &&
                address.getState().equals(state)
        ) {
            return true;
        }
        return false;
    }

    public String getSimple() {
        return country + " " + state + " " + city + " " + street + " (" + postal + ") ";
    }
}
