package com.example.ecommerce.model;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "history_db")
public class History {

    @ColumnInfo(name = "product_simple")
    String product;
    @ColumnInfo(name = "address_simple")
    String address;
    @ColumnInfo(name = "shipping_simple")
    String method;
    @ColumnInfo(name = "payment_simple")
    String payment;
    @PrimaryKey(autoGenerate = true)
    public long id;

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) return false;
        return ((History) obj).address.equals(address)
                && ((History) obj).payment.equals(payment)
                && ((History) obj).method.equals(method)
                && ((History) obj).product.equals(product);
    }

    public String getProduct() {
        return product;
    }

    public String getAddress() {
        return address;
    }

    public String getMethod() {
        return method;
    }

    public String getPayment() {
        return payment;
    }

    public History(String product, String address, String method, String payment) {
        this.product = product;
        this.address = address;
        this.method = method;
        this.payment = payment;
    }
}
