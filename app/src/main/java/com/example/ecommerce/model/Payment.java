package com.example.ecommerce.model;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "payment_db")
public class Payment {

    @ColumnInfo(name = "cardNumber")
    String cardNumber;
    @ColumnInfo(name = "cvv2")
    String cvv2;
    @ColumnInfo(name = "expiration")
    String expiration;
    @ColumnInfo(name = "billCountry")
    String billCountry;
    @ColumnInfo(name = "billPostal")
    String billPostal;
    @PrimaryKey(autoGenerate = true)
    long id;

    public String getCardNumber() {
        return cardNumber;
    }

    public String getCvv2() {
        return cvv2;
    }

    public String getExpiration() {
        return expiration;
    }

    public String getBillCountry() {
        return billCountry;
    }

    public String getBillPostal() {
        return billPostal;
    }

    public Payment(String cardNumber, String cvv2, String expiration, String billCountry, String billPostal) {
        this.cardNumber = cardNumber;
        this.cvv2 = cvv2;
        this.expiration = expiration;
        this.billCountry = billCountry;
        this.billPostal = billPostal;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) return false;
        Payment payment = (Payment) obj;
        return payment.cardNumber.equals(cardNumber) &&
                payment.cvv2.equals(cvv2) &&
                payment.expiration.equals(expiration) &&
                payment.billCountry.equals(billCountry) &&
                payment.billPostal.equals(billPostal);
    }

    public String getSimple() {
        return cardNumber + " " + billCountry + " " + billPostal;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
