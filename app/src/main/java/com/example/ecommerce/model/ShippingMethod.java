package com.example.ecommerce.model;

import androidx.annotation.Nullable;

public class ShippingMethod {

    String method;

    public ShippingMethod(String method) {
        this.method = method;
    }

    public String getMethod() {
        return method;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (obj == null) return false;
        return ((ShippingMethod) obj).method.equals(method);
    }
}
