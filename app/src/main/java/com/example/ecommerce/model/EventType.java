package com.example.ecommerce.model;

public enum EventType {

    NEXT_PAGE,
    PREV_PAGE,
    POP

}
