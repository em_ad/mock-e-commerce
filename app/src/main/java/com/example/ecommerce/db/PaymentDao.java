package com.example.ecommerce.db;

import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.ecommerce.model.Payment;

import java.util.List;

@Dao
public interface PaymentDao {
    @Query("SELECT * FROM payment_db")
    DataSource.Factory<Integer, Payment> getAll();

    @Query("SELECT * FROM payment_db")
    List<Payment> getAllSimple();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Payment> newsItems);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Payment newsItem);

    @Delete
    void delete(Payment item);

}
