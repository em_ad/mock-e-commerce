package com.example.ecommerce.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.ecommerce.model.Address;
import com.example.ecommerce.model.History;
import com.example.ecommerce.model.Payment;
import com.example.ecommerce.util.Constants;

@Database(entities = {Address.class, History.class, Payment.class}, version = Constants.DB_VERSION, exportSchema = false)
public abstract class DBInterface extends RoomDatabase {
    public abstract AddressDao addressDao();

    public abstract HistoryDao historyDao();

    public abstract PaymentDao paymentDao();
}

