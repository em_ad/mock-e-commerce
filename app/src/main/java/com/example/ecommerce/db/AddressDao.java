package com.example.ecommerce.db;


import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.ecommerce.model.Address;

import java.util.List;

@Dao
public interface AddressDao {

    @Query("SELECT * FROM address_db")
    DataSource.Factory<Integer, Address> getAll();

    @Query("SELECT * FROM address_db")
    List<Address> getAllSimple();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Address> newsItems);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Address newsItem);

    @Query("SELECT * FROM address_db WHERE postal=:postal ")
    Address loadSingle(int postal);

    @Delete
    void delete(Address item);

}
