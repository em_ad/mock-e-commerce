package com.example.ecommerce.db;

import androidx.room.Room;

import com.example.ecommerce.App;
import com.example.ecommerce.util.Constants;

public class DBAccess {

    private static DBAccess dBaccess = null;

    private DBInterface db;

    private DBAccess() {
        db = Room.databaseBuilder(App.getContext(), DBInterface.class, Constants.ROOM_NAME).allowMainThreadQueries().build();
        dBaccess = this;
    }

    public static DBAccess getInstance() {
        if (dBaccess == null)
            dBaccess = new DBAccess();
        return dBaccess;
    }

    public DBInterface getDb() {
        return db;
    }
}