package com.example.ecommerce.db;

import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.ecommerce.model.History;

import java.util.List;

@Dao
public interface HistoryDao {

    @Query("SELECT * FROM history_db")
    DataSource.Factory<Integer, History> getAll();

    @Query("SELECT * FROM history_db")
    List<History> getAllSimple();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<History> newsItems);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(History newsItem);

    @Query("SELECT * FROM history_db WHERE `id`=:id ")
    History loadSingle(int id);

    @Delete
    void delete(History item);

}
