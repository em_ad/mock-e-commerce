package com.example.ecommerce.util;

import android.content.res.Resources;
import android.widget.TextView;

import com.example.ecommerce.App;
import com.example.ecommerce.R;

public class AndroidUtils {

    static AndroidUtils instance;

    public static AndroidUtils getInstance() {
        if (instance == null)
            instance = new AndroidUtils();
        return instance;
    }

    public boolean hasContent(CharSequence et) {
        return et != null && et.toString().trim().length() > 0;
    }

    public float Px(Resources resources, float dip) {
        return dip * resources.getDisplayMetrics().density;
    }

    public void setTextSizeAndColor(Resources res, TextView tv) {
        tv.setTextSize(AndroidUtils.getInstance().Px(res, App.getPref().getInt(Constants.PREF_TEXT_SIZE, Constants.TEXT_SMALL)));
        tv.setTextColor((int) AndroidUtils.getInstance().Px(res, App.getPref().getInt(Constants.PREF_TEXT_COLOR, res.getColor(R.color.grey))));
    }

    public void setTextSizeAndColor(Resources res, TextView tv, boolean makeSmaller) {
        if (makeSmaller)
            tv.setTextSize(AndroidUtils.getInstance().Px(res, App.getPref().getInt(Constants.PREF_TEXT_SIZE, Constants.TEXT_SMALL) / 1.5f));
        tv.setTextColor((int) AndroidUtils.getInstance().Px(res, App.getPref().getInt(Constants.PREF_TEXT_COLOR, res.getColor(R.color.grey))));
    }

}
