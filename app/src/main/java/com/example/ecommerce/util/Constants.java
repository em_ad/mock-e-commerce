package com.example.ecommerce.util;

public class Constants {

    public static final int DB_VERSION = 1;
    public static final String ROOM_NAME = "ecommerce";
    public static final String APP_PREF_NAME = "E_Commerce_Pref_Name";
    public static final String PREF_THEME_NAME = "E_Commerce_Pref_Theme_Name";
    public static final String PREF_TEXT_COLOR = "E_Commerce_Pref_Text_Color";
    public static final String PREF_TEXT_SIZE = "E_Commerce_Pref_Text_Size";
    public static final int THEME_TWO = 2;
    public static final int DEFAULT_THEME = 0;
    public static final int THEME_ONE = -1;
    public static final int COLOR_BLACK = 0xfe000000;
    public static final int COLOR_WHITE = 0xffffffff;
    public static final int COLOR_GREY = 0xff999999;
    public static final int TEXT_SMALL = 5;
    public static final int TEXT_MEDIUM = 8;
    public static final int TEXT_LARGE = 11;
    public static final int TEXT_LARGER = 14;
}
