package com.example.ecommerce.util;

import android.content.res.Resources;

import com.example.ecommerce.R;
import com.example.ecommerce.model.Product;
import com.example.ecommerce.model.ShippingMethod;

import java.util.ArrayList;

public class Mocker {

    public static ArrayList<ShippingMethod> getShippingMethods() {
        ArrayList<ShippingMethod> list = new ArrayList<>();
        list.add(new ShippingMethod("با پیک"));
        list.add(new ShippingMethod("پست پیشتاز"));
        list.add(new ShippingMethod("سفارشی رایگان"));
        return list;
    }

    public static ArrayList<Product> getProducts(Resources res) {
        ArrayList<Product> products = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            products.add(
                    new Product(
                            res.getStringArray(R.array.goods)[i], Long.valueOf(String.valueOf((int) (Math.random() * 10000)))
                    ));
        }

        return products;
    }
}
