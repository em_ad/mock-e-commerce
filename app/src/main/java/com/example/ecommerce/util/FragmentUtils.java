package com.example.ecommerce.util;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.ecommerce.R;

import org.jetbrains.annotations.NotNull;

public class FragmentUtils {

    public static void addFragment(@NotNull FragmentManager fm, Fragment f) {
        fm.beginTransaction().add(R.id.flContainer, f, f.getClass().getSimpleName()).addToBackStack(f.getClass().getSimpleName()).commitAllowingStateLoss();
    }

    public static void openFragment(@NotNull FragmentManager fm, Fragment f) {
        for (int i = 0; i < fm.getFragments().size(); i++) {
            fm.popBackStackImmediate();
        }

        fm.beginTransaction()
                .replace(R.id.flContainer, f, f.getClass().getSimpleName())
                .addToBackStack(f.getClass().getSimpleName())
                .commitAllowingStateLoss();
    }

    public static void popFragment(@NotNull FragmentManager fm) {
        fm.popBackStack();
    }

    public static void popSepcificFragment(@NotNull FragmentManager fm, String tag) {
        Fragment fragment = fm.findFragmentByTag(tag);
        if (fragment != null)
            fm.beginTransaction().remove(fragment).commit();
    }

    public static boolean checkFragmentExistence(@NotNull FragmentManager fm, Fragment f) {
        for (int i = 0; i < fm.getFragments().size(); i++) {
            if (fm.getFragments().get(i).getClass().getSimpleName().equals(f.getClass().getSimpleName()))
                return true;
        }
        return false;
    }

    public static void popUntilFind(@NotNull FragmentManager fm, Fragment f) {
        for (int i = fm.getFragments().size() - 1; i >= 0; i--) {
            if (!fm.getFragments().get(i).getClass().getSimpleName().equals(f.getClass().getSimpleName()))
                popSepcificFragment(fm, f.getClass().getSimpleName());
        }
    }

    public static void removeTimes(FragmentManager supportFragmentManager, int times) {
        for (int i = 0; i < times; i++) {
            try {
                FragmentUtils.popFragment(supportFragmentManager);
            } catch (Exception e) {
                break;
            }
        }
    }
}
