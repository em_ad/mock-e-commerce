package com.example.ecommerce.ui.purchase.adapter;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecommerce.R;
import com.example.ecommerce.model.Product;
import com.example.ecommerce.ui.purchase.callback.ItemSelectedInterface;
import com.example.ecommerce.util.AndroidUtils;
import com.example.ecommerce.util.Mocker;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private ArrayList<Product> data;
    private ItemSelectedInterface callback;
    private Resources res;

    public ProductAdapter(Resources res, ItemSelectedInterface callback) {
        this.data = Mocker.getProducts(res);
        this.callback = callback;
        this.res = res;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_square, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvProduct.setText(data.get(position).getName() + "\n" + data.get(position).getPrice() + " تومان");
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvProduct;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvProduct = itemView.findViewById(R.id.tvProduct);
            AndroidUtils.getInstance().setTextSizeAndColor(res, tvProduct, true);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.clicked(getAdapterPosition());
                }
            });
        }
    }
}
