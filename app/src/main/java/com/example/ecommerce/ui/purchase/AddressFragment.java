package com.example.ecommerce.ui.purchase;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecommerce.BaseFragment;
import com.example.ecommerce.R;
import com.example.ecommerce.model.Address;
import com.example.ecommerce.model.Product;
import com.example.ecommerce.ui.purchase.adapter.AddressAdapter;
import com.example.ecommerce.ui.purchase.callback.ItemSelectedOrRemovedInterface;
import com.example.ecommerce.util.AndroidUtils;
import com.example.ecommerce.util.FragmentUtils;
import com.example.ecommerce.vm.AddressViewModel;

public class AddressFragment extends BaseFragment {

    private Product product;
    private AddressAdapter adapter;
    private AddressViewModel addressViewModel;
    private EditText etCountry;
    private EditText etState;
    private EditText etCity;
    private EditText etStreet;
    private EditText etPostal;
    private Switch sSave;

    public static AddressFragment newInstance(Product product) {
        AddressFragment fragment = new AddressFragment();
        fragment.product = product;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_address, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addressViewModel = ViewModelProviders.of(this).get(AddressViewModel.class);
        findViews(view);
        addressViewModel.exposeList().observe(this, new Observer<PagedList<Address>>() {
            @Override
            public void onChanged(PagedList<Address> addresses) {
                adapter.submitList(addresses);
            }
        });
    }

    private void findViews(View view) {

        etCountry = view.findViewById(R.id.etCountry);
        setTextSizeAndColor(etCountry);
        etState = view.findViewById(R.id.etState);
        setTextSizeAndColor(etState);
        etCity = view.findViewById(R.id.etCity);
        setTextSizeAndColor(etCity);
        etPostal = view.findViewById(R.id.etPostal);
        setTextSizeAndColor(etPostal);
        etStreet = view.findViewById(R.id.etStreet);
        setTextSizeAndColor(etStreet);
        sSave = view.findViewById(R.id.sSave);
        setTextSizeAndColor(sSave);
        setTextSizeAndColor(view.findViewById(R.id.pageHeader));
        setTextSizeAndColor(view.findViewById(R.id.pageFooter));
        RecyclerView recycler = view.findViewById(R.id.recycler);
        adapter = new AddressAdapter(new ItemSelectedOrRemovedInterface() {
            @Override
            public void clicked(int position) {
                setData(position);
            }

            @Override
            public void removed(int position) {
                addressViewModel.removeAddress(position);
            }
        });
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        recycler.setAdapter(adapter);

    }

    private void setData(int position) {
        Address item = addressViewModel.exposeSingleAddress(position);
        if (item == null)
            return;
        etCountry.setText(item.getCountry());
        etStreet.setText(item.getStreet());
        etCity.setText(item.getCity());
        etState.setText(item.getState());
        etPostal.setText(item.getPostal());
        sSave.setChecked(true);
    }

    @Override
    public void next(Object load) {
        try {
            super.next(load);
        } catch (Exception e) {
            return;
        }
        if (!AndroidUtils.getInstance().hasContent(etCountry.getText()) ||
                !AndroidUtils.getInstance().hasContent(etStreet.getText()) ||
                !AndroidUtils.getInstance().hasContent(etCity.getText()) ||
                !AndroidUtils.getInstance().hasContent(etState.getText()) ||
                !AndroidUtils.getInstance().hasContent(etPostal.getText())) { // || etPostal.getText().toString().length() < 10
            Toast.makeText(getContext(), "لطفا اطلاعات آدرس را تکمیل کنید", Toast.LENGTH_SHORT).show();
            return;
        }
        Address item = new Address(etCountry.getText().toString().trim(),
                etState.getText().toString().trim(),
                etCity.getText().toString().trim(),
                etStreet.getText().toString().trim(),
                etPostal.getText().toString().trim());
        if (sSave.isChecked()) {
            addressViewModel.saveItem(item);
        }
        if (getActivity() != null)
            FragmentUtils.addFragment(getActivity().getSupportFragmentManager(), ShippingMethodFragment.newInstance(product, item));

    }

    @Override
    public void prev(Object payload) {
        if (isAdded()) {
            FragmentUtils.popFragment(getChildFragmentManager());
        }
    }

}
