package com.example.ecommerce.ui.purchase;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecommerce.BaseFragment;
import com.example.ecommerce.R;
import com.example.ecommerce.model.Address;
import com.example.ecommerce.model.History;
import com.example.ecommerce.model.Payment;
import com.example.ecommerce.model.Product;
import com.example.ecommerce.model.ShippingMethod;
import com.example.ecommerce.ui.purchase.adapter.PaymentAdapter;
import com.example.ecommerce.ui.purchase.callback.ItemSelectedOrRemovedInterface;
import com.example.ecommerce.util.AndroidUtils;
import com.example.ecommerce.util.FragmentUtils;
import com.example.ecommerce.vm.HistoryViewModel;
import com.example.ecommerce.vm.PaymentViewModel;

public class PaymentFragment extends BaseFragment {

    private PaymentViewModel paymentViewModel;
    private PaymentAdapter adapter;

    private Address selectedAddress;
    private Product product;
    private ShippingMethod selectedMethod;

    private EditText etCardNumber;
    private EditText etPassword;
    private EditText etCVV;
    private EditText etExpiration;
    private EditText etCountry;
    private EditText etPostal;

    private Switch sSave;

    public PaymentFragment() {
    }

    public static PaymentFragment newInstance(Product product, Address selectedAddress, ShippingMethod selectedMethod) {
        PaymentFragment fragment = new PaymentFragment();
        fragment.product = product;
        fragment.selectedAddress = selectedAddress;
        fragment.selectedMethod = selectedMethod;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_payment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        paymentViewModel = ViewModelProviders.of(this).get(PaymentViewModel.class);
        findViews(view);
    }

    private void findViews(View view) {

        etCardNumber = view.findViewById(R.id.etCardNumber);
        setTextSizeAndColor(etCardNumber);
        etPassword = view.findViewById(R.id.etPassword);
        setTextSizeAndColor(etPassword);
        etCVV = view.findViewById(R.id.etCVV);
        setTextSizeAndColor(etCVV);
        etExpiration = view.findViewById(R.id.etExpiration);
        setTextSizeAndColor(etExpiration);
        etCountry = view.findViewById(R.id.etCountry);
        setTextSizeAndColor(etCountry);
        etPostal = view.findViewById(R.id.etPostal);
        RecyclerView recycler = view.findViewById(R.id.recycler);
        sSave = view.findViewById(R.id.sSave);
        setTextSizeAndColor(sSave);
        setTextSizeAndColor(view.findViewById(R.id.pageHeader));
        setTextSizeAndColor(view.findViewById(R.id.pageHeader2));
        setTextSizeAndColor(view.findViewById(R.id.pageFooter));

        adapter = new PaymentAdapter(new ItemSelectedOrRemovedInterface() {
            @Override
            public void clicked(int position) {
                setData(position);
            }

            @Override
            public void removed(int position) {
                paymentViewModel.removePayment(position);
            }
        });
        paymentViewModel.exposeList().observe(this, new Observer<PagedList<Payment>>() {
            @Override
            public void onChanged(PagedList<Payment> payments) {
                adapter.submitList(payments);
            }
        });
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void setData(int position) {
        Payment item = paymentViewModel.exposeSinglePayment(position);
        if (item == null)
            return;
        etCardNumber.setText(item.getCardNumber());
        etPassword.setError("رمز را وارد کنید");
        etPassword.requestFocus();
        etCVV.setText(item.getCvv2());
        etExpiration.setText(item.getExpiration());
        etCountry.setText(item.getBillCountry());
        etPostal.setText(item.getBillPostal());
    }

    @Override
    public void prev(Object load) {
        if (getActivity() != null)
            FragmentUtils.popFragment(getChildFragmentManager());
    }

    @Override
    public void next(Object load) {
        try {
            super.next(load);
        } catch (Exception e) {
            return;
        }
        if (!AndroidUtils.getInstance().hasContent(etCardNumber.getText()) ||
                !AndroidUtils.getInstance().hasContent(etPassword.getText()) ||
                !AndroidUtils.getInstance().hasContent(etCVV.getText()) ||
                !AndroidUtils.getInstance().hasContent(etExpiration.getText()) ||
                !AndroidUtils.getInstance().hasContent(etCountry.getText()) ||
                !AndroidUtils.getInstance().hasContent(etPostal.getText())) {
            Toast.makeText(getContext(), "لطفا اطلاعات پرداخت را کامل کنید", Toast.LENGTH_SHORT).show();
            return;
        }
        Payment payment = new Payment(etCardNumber.getText().toString(),
                etCVV.getText().toString(),
                etExpiration.getText().toString(),
                etCountry.getText().toString(),
                etPostal.getText().toString());
        if (sSave.isChecked()) {
            paymentViewModel.saveNewPayment(payment);
        }
        History paymentHistory = new History(product.getName(), selectedAddress.getSimple(), selectedMethod.getMethod(), payment.getSimple());
        ViewModelProviders.of(this).get(HistoryViewModel.class).saveNewHistory(paymentHistory);
        Toast.makeText(getContext(), "پرداخت با موفقیت انچام شد", Toast.LENGTH_SHORT).show();
        FragmentUtils.removeTimes(getActivity().getSupportFragmentManager(), 3);
//        FragmentUtils.popUntilFind(getActivity().getSupportFragmentManager(), ProductsFragment.newInstance());
    }
}
