package com.example.ecommerce.ui.purchase.callback;

public interface ItemSelectedOrRemovedInterface {
    void clicked(int position);

    void removed(int position);
}
