package com.example.ecommerce.ui.purchase;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecommerce.BaseFragment;
import com.example.ecommerce.R;
import com.example.ecommerce.ui.purchase.adapter.ProductAdapter;
import com.example.ecommerce.ui.purchase.callback.ItemSelectedInterface;
import com.example.ecommerce.util.FragmentUtils;
import com.example.ecommerce.util.Mocker;

public class ProductsFragment extends BaseFragment {

    public static ProductsFragment newInstance() {
        return new ProductsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_products, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViews(view);
    }

    @Override
    public void next(Object load) {
        try {
            super.next(load);
        } catch (Exception e) {
            return;
        }
    }

    private void findViews(View view) {
        RecyclerView recycler = view.findViewById(R.id.recycler);
        ProductAdapter adapter = new ProductAdapter(getResources(), new ItemSelectedInterface() {
            @Override
            public void clicked(int position) {
                if (getActivity() != null)
                    FragmentUtils.addFragment(getActivity().getSupportFragmentManager(), AddressFragment.newInstance(Mocker.getProducts(getResources()).get(position)));
            }
        });
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new GridLayoutManager(getContext(), 3));
    }

    @Override
    public void prev(Object load) {
        if (getActivity() != null)
            FragmentUtils.popFragment(getActivity().getSupportFragmentManager());
    }

}
