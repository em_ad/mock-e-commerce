package com.example.ecommerce.ui.purchase;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecommerce.BaseFragment;
import com.example.ecommerce.R;
import com.example.ecommerce.model.Address;
import com.example.ecommerce.model.Product;
import com.example.ecommerce.model.ShippingMethod;
import com.example.ecommerce.ui.purchase.adapter.ShippingMethodAdapter;
import com.example.ecommerce.ui.purchase.callback.ItemSelectedInterface;
import com.example.ecommerce.util.FragmentUtils;
import com.example.ecommerce.util.Mocker;

public class ShippingMethodFragment extends BaseFragment {

    private Address selectedAddress;
    private Product product;
    private ShippingMethod selectedMethod = null;

    public static ShippingMethodFragment newInstance(Product product, Address selectedAddress) {
        ShippingMethodFragment fragment = new ShippingMethodFragment();
        fragment.selectedAddress = selectedAddress;
        fragment.product = product;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_shipping_method, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViews(view);
    }

    private void findViews(View view) {
        RecyclerView recycler = view.findViewById(R.id.recycler);
        recycler.setAdapter(new ShippingMethodAdapter(Mocker.getShippingMethods(), new ItemSelectedInterface() {
            @Override
            public void clicked(int position) {
                selectedMethod = Mocker.getShippingMethods().get(position);
            }
        }));
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void prev(Object load) {
        if (isAdded())
            FragmentUtils.popFragment(getChildFragmentManager());
    }

    @Override
    public void next(Object load) {
        try {
            super.next(load);
        } catch (Exception e) {
            return;
        }
        if (selectedMethod == null) {
            Toast.makeText(getContext(), "لطفا یک شیوه ارسال انتخاب کنید", Toast.LENGTH_SHORT).show();
            return;
        }
        FragmentUtils.addFragment(getActivity().getSupportFragmentManager(), PaymentFragment.newInstance(product, selectedAddress, selectedMethod));
    }
}
