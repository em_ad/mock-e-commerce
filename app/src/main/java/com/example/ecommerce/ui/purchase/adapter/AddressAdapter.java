package com.example.ecommerce.ui.purchase.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecommerce.R;
import com.example.ecommerce.model.Address;
import com.example.ecommerce.ui.purchase.callback.ItemSelectedOrRemovedInterface;
import com.example.ecommerce.util.AndroidUtils;

public class AddressAdapter extends PagedListAdapter<Address, AddressAdapter.ViewHolder> {

    private ItemSelectedOrRemovedInterface callback;
    Context context;

    public AddressAdapter(ItemSelectedOrRemovedInterface callback) {
        super(new DiffUtil.ItemCallback<Address>() {
            @Override
            public boolean areItemsTheSame(@NonNull Address oldItem, @NonNull Address newItem) {
                return oldItem.getPostal().equals(newItem.getPostal());
            }

            @Override
            public boolean areContentsTheSame(@NonNull Address oldItem, @NonNull Address newItem) {
                return oldItem.equals(newItem);
            }
        });
        this.callback = callback;
    }

    @NonNull
    @Override
    public AddressAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_address_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AddressAdapter.ViewHolder holder, int position) {
        if (getItem(position) != null) {
            holder.tvAddress.setText(getItem(position).getSimple());
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvAddress;
        ImageView ivDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            ivDelete = itemView.findViewById(R.id.ivDelete);
            AndroidUtils.getInstance().setTextSizeAndColor(context.getResources(), tvAddress);
            tvAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.clicked(getAdapterPosition());
                }
            });

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.removed(getAdapterPosition());
                }
            });
        }
    }
}
