package com.example.ecommerce.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import com.example.ecommerce.App;
import com.example.ecommerce.R;
import com.example.ecommerce.ui.history.HistoryFragment;
import com.example.ecommerce.ui.purchase.ProductsFragment;
import com.example.ecommerce.ui.settings.SettingsFragment;
import com.example.ecommerce.util.Constants;
import com.example.ecommerce.util.FragmentUtils;
import com.example.ecommerce.vm.FlowViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mTopToolbar;
    private ImageView ivBack;
    private BottomNavigationView navbar;

    FlowViewModel flowViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        switch (App.getPref().getInt(Constants.PREF_THEME_NAME, Constants.THEME_ONE)) {
            case Constants.THEME_ONE:
                setTheme(R.style.AppTheme);
                break;
            case Constants.THEME_TWO:
                setTheme(R.style.AppThemeLight);
                break;
        }
        setContentView(R.layout.activity_main);
        findViews();
        setSupportActionBar(mTopToolbar);
        BottomNavigationView.OnNavigationItemSelectedListener listener = menuItem -> {
            int id = menuItem.getItemId();
            switch (id) {
                case R.id.home:
                    FragmentUtils.openFragment(getSupportFragmentManager(), ProductsFragment.newInstance());
                    break;
                case R.id.settings:
                    FragmentUtils.openFragment(getSupportFragmentManager(), SettingsFragment.newInstance());
                    break;
                case R.id.history:
                    FragmentUtils.openFragment(getSupportFragmentManager(), HistoryFragment.newInstance());
                    break;
            }
            return false;
        };
        navbar.setOnNavigationItemSelectedListener(listener);
        FragmentUtils.openFragment(getSupportFragmentManager(), ProductsFragment.newInstance());
        flowViewModel = ViewModelProviders.of(this).get(FlowViewModel.class);
    }

    private void findViews() {
        mTopToolbar = findViewById(R.id.my_toolbar);
        ivBack = findViewById(R.id.back);
        ImageView ivNext = findViewById(R.id.done);
        navbar = findViewById(R.id.navigationView);
        ivBack.setOnClickListener(this);
        ivNext.setOnClickListener(this);
    }

    private void prevPage() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1)
            flowViewModel.postPrev();
    }

    private void nextPage() {
        flowViewModel.postNext();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                prevPage();
                break;
            case R.id.done:
                nextPage();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1)
            finishAffinity();
        super.onBackPressed();
    }
}

