package com.example.ecommerce.ui.purchase.callback;

public interface ItemSelectedInterface {
    void clicked(int position);
}
