package com.example.ecommerce.ui.purchase.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecommerce.R;
import com.example.ecommerce.model.ShippingMethod;
import com.example.ecommerce.ui.purchase.callback.ItemSelectedInterface;
import com.example.ecommerce.util.AndroidUtils;

import java.util.ArrayList;

public class ShippingMethodAdapter extends RecyclerView.Adapter<ShippingMethodAdapter.ViewHolder> {

    ArrayList<ShippingMethod> data;
    RadioButton lastSelected;
    ItemSelectedInterface callback;
    Context context;

    public ShippingMethodAdapter(ArrayList<ShippingMethod> data, ItemSelectedInterface callback) {
        this.data = data;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shipping_method_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvMethod.setText(data.get(position).getMethod());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RadioButton rbMethod;
        TextView tvMethod;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rbMethod = itemView.findViewById(R.id.rbMethod);
            tvMethod = itemView.findViewById(R.id.tvMethod);
            AndroidUtils.getInstance().setTextSizeAndColor(context.getResources(), tvMethod);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (lastSelected != null)
                        lastSelected.setChecked(false);
                    rbMethod.setChecked(true);
                    callback.clicked(getAdapterPosition());
                    lastSelected = rbMethod;
                }
            });
        }
    }
}
