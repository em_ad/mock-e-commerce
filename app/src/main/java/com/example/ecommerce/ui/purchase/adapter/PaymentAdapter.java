package com.example.ecommerce.ui.purchase.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecommerce.R;
import com.example.ecommerce.model.Payment;
import com.example.ecommerce.ui.purchase.callback.ItemSelectedOrRemovedInterface;
import com.example.ecommerce.util.AndroidUtils;

public class PaymentAdapter extends PagedListAdapter<Payment, PaymentAdapter.ViewHolder> {

    private ItemSelectedOrRemovedInterface callback;
    Context context;

    public PaymentAdapter(ItemSelectedOrRemovedInterface callback) {
        super(new DiffUtil.ItemCallback<Payment>() {
            @Override
            public boolean areItemsTheSame(@NonNull Payment oldItem, @NonNull Payment newItem) {
                return false;
            }

            @Override
            public boolean areContentsTheSame(@NonNull Payment oldItem, @NonNull Payment newItem) {
                return false;
            }
        });
        this.callback = callback;
    }

    @NonNull
    @Override
    public PaymentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        return new PaymentAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (getItem(position) != null) {
            holder.tvPayment.setText(getItem(position).getSimple());
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvPayment;
        ImageView ivDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvPayment = itemView.findViewById(R.id.tvPayment);
            ivDelete = itemView.findViewById(R.id.ivDelete);
            AndroidUtils.getInstance().setTextSizeAndColor(context.getResources(), tvPayment);

            tvPayment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.clicked(getAdapterPosition());
                }
            });

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.removed(getAdapterPosition());
                }
            });
        }
    }
}
