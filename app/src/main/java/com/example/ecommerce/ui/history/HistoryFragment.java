package com.example.ecommerce.ui.history;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecommerce.BaseFragment;
import com.example.ecommerce.R;
import com.example.ecommerce.model.History;
import com.example.ecommerce.ui.history.adapter.HistoryAdapter;
import com.example.ecommerce.vm.HistoryViewModel;


public class HistoryFragment extends BaseFragment {

    private HistoryViewModel historyViewModel;
    private RecyclerView recycler;
    private HistoryAdapter adapter;

    public HistoryFragment() {
    }

    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        historyViewModel = ViewModelProviders.of(this).get(HistoryViewModel.class);
        recycler = view.findViewById(R.id.recycler);
        adapter = new HistoryAdapter();
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        historyViewModel.exposeLiveData().observe(this, new Observer<PagedList<History>>() {
            @Override
            public void onChanged(PagedList<History> histories) {
                adapter.submitList(histories);
            }
        });
    }

    @Override
    public void next(Object load) {
        try {
            super.next(load);
        } catch (Exception e) {
            return;
        }
    }

    @Override
    public void prev(Object load) {

    }
}
