package com.example.ecommerce.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ecommerce.App;
import com.example.ecommerce.BaseFragment;
import com.example.ecommerce.R;
import com.example.ecommerce.ui.MainActivity;
import com.example.ecommerce.util.Constants;

import org.jetbrains.annotations.NotNull;

public class SettingsFragment extends BaseFragment {

    Switch sTheme;
    Spinner sTextSize;
    Spinner sTextColor;
    boolean isBlue = true;

    public SettingsFragment() {
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViews(view);
        if (App.getPref().getInt(Constants.PREF_THEME_NAME, Constants.THEME_ONE) == Constants.THEME_TWO) {
            sTheme.setChecked(true);
            isBlue = false;
        }
        switch (App.getPref().getInt(Constants.PREF_TEXT_COLOR, Constants.COLOR_WHITE)) {
            case Constants.COLOR_GREY:
                sTextColor.setSelection(0);
                break;
            case Constants.COLOR_WHITE:
                sTextColor.setSelection(1);
                break;
            case Constants.COLOR_BLACK:
                sTextColor.setSelection(2);
                break;
        }
        switch (App.getPref().getInt(Constants.PREF_TEXT_SIZE, Constants.TEXT_SMALL)) {
            case Constants.TEXT_SMALL:
                sTextSize.setSelection(0);
                break;
            case Constants.TEXT_MEDIUM:
                sTextSize.setSelection(1);
                break;
            case Constants.TEXT_LARGE:
                sTextSize.setSelection(2);
                break;
            case Constants.TEXT_LARGER:
                sTextSize.setSelection(3);
                break;
        }
        sTheme.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isBlue = !b;
            }
        });
    }

    private void findViews(View view) {

        sTheme = view.findViewById(R.id.sTheme);
        setTextSizeAndColor(sTheme);
        sTextColor = view.findViewById(R.id.textColorSpinner);
        sTextSize = view.findViewById(R.id.textSizeSpinner);

        ArrayAdapter adapterColor = ArrayAdapter.createFromResource(getContext(),
                R.array.text_colors, R.layout.item_settings_spinner);
        ArrayAdapter adapterSize = ArrayAdapter.createFromResource(getContext(),
                R.array.text_sizes, R.layout.item_settings_spinner);

        adapterSize.setDropDownViewResource(R.layout.item_settings_spinner);
        adapterColor.setDropDownViewResource(R.layout.item_settings_spinner);
        sTextColor.setAdapter(adapterColor);
        sTextSize.setAdapter(adapterSize);
    }

    @Override
    public void next(Object load) {
        try {
            super.next(load);
        } catch (Exception e) {
            return;
        }
        if (isBlue)
            App.editPref().putInt(Constants.PREF_THEME_NAME, Constants.THEME_ONE).apply();
        else
            App.editPref().putInt(Constants.PREF_THEME_NAME, Constants.THEME_TWO).apply();
        switch (sTextColor.getSelectedItemPosition()) {
            case 0:
                App.editPref().putInt(Constants.PREF_TEXT_COLOR, Constants.COLOR_GREY).apply();
                break;
            case 1:
                App.editPref().putInt(Constants.PREF_TEXT_COLOR, Constants.COLOR_WHITE).apply();
                break;
            case 2:
                App.editPref().putInt(Constants.PREF_TEXT_COLOR, Constants.COLOR_BLACK).apply();
                break;
        }
        switch (sTextSize.getSelectedItemPosition()) {
            case 0:
                App.editPref().putInt(Constants.PREF_TEXT_SIZE, Constants.TEXT_SMALL).apply();
                break;
            case 1:
                App.editPref().putInt(Constants.PREF_TEXT_SIZE, Constants.TEXT_MEDIUM).apply();
                break;
            case 2:
                App.editPref().putInt(Constants.PREF_TEXT_SIZE, Constants.TEXT_LARGE).apply();
                break;
            case 3:
                App.editPref().putInt(Constants.PREF_TEXT_SIZE, Constants.TEXT_LARGER).apply();
                break;
        }
        getActivity().finishAffinity();
        getActivity().startActivity(new Intent(getActivity(), MainActivity.class));
    }

    @Override
    public void prev(Object load) {

    }
}
