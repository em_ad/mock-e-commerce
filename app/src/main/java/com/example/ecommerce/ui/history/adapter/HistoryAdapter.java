package com.example.ecommerce.ui.history.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecommerce.R;
import com.example.ecommerce.model.History;
import com.example.ecommerce.util.AndroidUtils;

public class HistoryAdapter extends PagedListAdapter<History, HistoryAdapter.ViewHolder> {

    Context context;

    public HistoryAdapter() {
        super(new DiffUtil.ItemCallback<History>() {
            @Override
            public boolean areItemsTheSame(@NonNull History oldItem, @NonNull History newItem) {
                return oldItem.getId() == newItem.getId();
            }

            @Override
            public boolean areContentsTheSame(@NonNull History oldItem, @NonNull History newItem) {
                return oldItem.equals(newItem);
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        History history = getItem(position);
        if (history == null)
            return;
        holder.tvAddress.setText("آدرس: " + history.getAddress());
        holder.tvMethod.setText("شیوه ارسال: " + history.getMethod());
        holder.tvProduct.setText("خرید محصول " + history.getProduct());
        holder.tvPayment.setText("پرداخت با شماره کارت " + history.getPayment());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvProduct;
        TextView tvPayment;
        TextView tvAddress;
        TextView tvMethod;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvProduct = itemView.findViewById(R.id.tvProduct);
            AndroidUtils.getInstance().setTextSizeAndColor(context.getResources(), tvProduct);
            tvPayment = itemView.findViewById(R.id.tvPayment);
            AndroidUtils.getInstance().setTextSizeAndColor(context.getResources(), tvPayment);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            AndroidUtils.getInstance().setTextSizeAndColor(context.getResources(), tvAddress);
            tvMethod = itemView.findViewById(R.id.tvMethod);
            AndroidUtils.getInstance().setTextSizeAndColor(context.getResources(), tvMethod);

        }
    }
}
