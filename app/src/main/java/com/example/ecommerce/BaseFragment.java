package com.example.ecommerce;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.ecommerce.model.Event;
import com.example.ecommerce.util.AndroidUtils;
import com.example.ecommerce.util.Constants;
import com.example.ecommerce.vm.FlowViewModel;

public abstract class BaseFragment extends Fragment {

    FlowViewModel viewModel;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(FlowViewModel.class);
        viewModel.exposeLiveData().observe(this, new Observer<Event>() {
            @Override
            public void onChanged(Event event) {
                try {
                    switch (event.getType()) {
                        case NEXT_PAGE:
                            next(event.getPayload());
                            break;
                        case PREV_PAGE:
                            prev(event.getPayload());
                            break;
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    public void next(Object payload) throws Exception {
        if (!isAdded() || getContext() == null || !((String) payload).equals(this.getClass().getSimpleName())){
            throw new Exception();
        }
    }

    public abstract void prev(Object payload);

    protected void setTextSizeAndColor(TextView tv) {
        try {
            tv.setTextSize(AndroidUtils.getInstance().Px(getResources(), App.getPref().getInt(Constants.PREF_TEXT_SIZE, Constants.TEXT_SMALL)));
        } catch (Exception e) {
            return;
        }
        try {
            tv.setTextColor(App.getPref().getInt(Constants.PREF_TEXT_COLOR, getResources().getColor(R.color.white)));
        } catch (Exception e) {
            return;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        App.setActiveFragment(this);
    }
}
