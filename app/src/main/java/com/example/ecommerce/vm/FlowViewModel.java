package com.example.ecommerce.vm;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.ecommerce.App;
import com.example.ecommerce.model.Event;
import com.example.ecommerce.model.EventType;

public class FlowViewModel extends ViewModel {

    private MutableLiveData<Event> eventMutableLiveData;

    public FlowViewModel() {
        eventMutableLiveData = new MutableLiveData<>();
    }

    public void postPrev() {
        eventMutableLiveData.postValue(new Event(EventType.PREV_PAGE, App.getActiveFragment().getClass().getSimpleName()));
    }

    public void postNext() {
        eventMutableLiveData.postValue(new Event(EventType.NEXT_PAGE, App.getActiveFragment().getClass().getSimpleName()));
    }

    public MutableLiveData<Event> exposeLiveData() {
        return eventMutableLiveData;
    }

}
