package com.example.ecommerce.vm;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.example.ecommerce.db.DBAccess;
import com.example.ecommerce.model.Address;

public class AddressViewModel extends ViewModel {

    private LiveData<PagedList<Address>> addressLiveData;

    public AddressViewModel() {
        getDataFromDB();
    }

    private void getDataFromDB() {
        addressLiveData = new LivePagedListBuilder<>(DBAccess.getInstance().getDb().addressDao().getAll(), 5).build();
    }

    public void removeAddress(int address) {
        DBAccess.getInstance().getDb().addressDao().delete(addressLiveData.getValue().get(address));
    }

    public Address exposeSingleAddress(int pos) {
        return addressLiveData.getValue().get(pos);
    }

    public LiveData<PagedList<Address>> exposeList() {
        return addressLiveData;
    }

    public void saveItem(Address item) {
        DBAccess.getInstance().getDb().addressDao().insert(item);
    }
}
