package com.example.ecommerce.vm;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.example.ecommerce.db.DBAccess;
import com.example.ecommerce.model.History;

public class HistoryViewModel extends ViewModel {

    LiveData<PagedList<History>> listLiveData;

    public HistoryViewModel() {
        listLiveData = new LivePagedListBuilder<>(DBAccess.getInstance().getDb().historyDao().getAll(), 5).build();
    }

    public LiveData<PagedList<History>> exposeLiveData() {
        return listLiveData;
    }

    public void saveNewHistory(History paymentHistory) {
        DBAccess.getInstance().getDb().historyDao().insert(paymentHistory);
    }
}
