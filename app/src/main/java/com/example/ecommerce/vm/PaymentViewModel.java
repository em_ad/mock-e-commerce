package com.example.ecommerce.vm;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.example.ecommerce.db.DBAccess;
import com.example.ecommerce.model.Payment;

public class PaymentViewModel extends ViewModel {

    private LiveData<PagedList<Payment>> LiveData;

    public PaymentViewModel() {
        getDataFromDB();
    }

    private void getDataFromDB() {
        LiveData = new LivePagedListBuilder<>(DBAccess.getInstance().getDb().paymentDao().getAll(), 5).build();
    }

    public void removePayment(int pos) {
        DBAccess.getInstance().getDb().paymentDao().delete(LiveData.getValue().get(pos));
    }

    public Payment exposeSinglePayment(int pos) {
        return LiveData.getValue().get(pos);
    }

    public LiveData<PagedList<Payment>> exposeList() {
        return LiveData;
    }

    public void saveNewPayment(Payment payment) {
        DBAccess.getInstance().getDb().paymentDao().insert(payment);
    }
}
