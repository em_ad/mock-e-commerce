package com.example.ecommerce;

import android.app.Application;
import android.content.SharedPreferences;

import com.example.ecommerce.util.Constants;

public class App extends Application {
    private static Application app;

    public static Application getContext() {
        return app;
    }

    private static SharedPreferences pref;

    @Override
    public void onCreate() {
        super.onCreate();
        this.app = this;
        this.pref = getSharedPreferences(Constants.APP_PREF_NAME, MODE_PRIVATE);
    }

    public static SharedPreferences.Editor editPref() {
        return pref.edit();
    }

    public static SharedPreferences getPref() {
        return pref;
    }

    private static BaseFragment activeFragment;

    public static BaseFragment getActiveFragment() {
        return activeFragment;
    }

    public static void setActiveFragment(BaseFragment active) {
        activeFragment = active;
    }
}
