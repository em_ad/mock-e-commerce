package com.example.ecommerce;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.example.ecommerce.ui.MainActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    ActivityTestRule<MainActivity> testRule = new ActivityTestRule<>(MainActivity.class);

    @Mock
    MainActivity activity;

    @Before
    public void setup() {
        activity = testRule.getActivity();
    }

    @After
    public void tearDown() {
        activity = null;
    }

    @Test
    public void checkFragmentCreation() {
        System.out.println(activity.getSupportFragmentManager().getBackStackEntryCount());
    }

}
